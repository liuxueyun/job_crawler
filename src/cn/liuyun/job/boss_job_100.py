import requests
import xlwt
from bs4 import BeautifulSoup


def request_boss(url):
    headers = {
        'Cookie': 'lastCity=101040100; __zp_seo_uuid__=be529076-7a17-436a-8257-bcd166096a14; __g=-; Hm_lvt_194df3105ad7148dcf2b98a91b5e727a=1641723387; __l=r=https%3A%2F%2Fwww.baidu.com%2Flink%3Furl%3DLJg0m5sTRDC7RRIenrM8TASK8EDAA_PTvVHstOwbMr3YWsdgHMkVzro6VY0Dz_PoqM7WmyHKDear-24wMxzD5ZHU7TN69GC8gukqNCeITPK%26wd%3D%26eqid%3Dee47b9be0003789e000000066040d409&l=%2Fwww.zhipin.com%2Fjob_detail%2F&s=3&friend_source=0&s=3&friend_source=0; acw_tc=0bdd34ba16420029507135053e01a7d6c5a66bd81cfa08a84603f72677c777; __c=1610261417; __a=45028795.1610261417..1610261417.58.1.58.58; Hm_lpvt_194df3105ad7148dcf2b98a91b5e727a=1642004624; __zp_stoken__=357fdaXpEV1liVT0scTggLi4tdXlzJkppbUx%2BIC5iZHw1IEsFNHICDEd9I15HbFd0D3Z%2BelBVNgNuRzwVIlFdKhZsTHQqF0oSZQIDWhkjdlJNETo0GyZFZxQpFhMUVAQ%2FTU9MDi1fN3RRXTk%3D',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36',
    }

    try:
        response = requests.get(url=url, headers=headers)
        if response.status_code == 200:
            return response.text
    except requests.RequestException:
        return None


book = xlwt.Workbook(encoding='utf-8', style_compression=0)

sheet = book.add_sheet('工作岗位', cell_overwrite_ok=True)
sheet.write(0, 0, '岗位名称')
sheet.write(0, 1, '工作地址')
sheet.write(0, 2, '薪资范围')
sheet.write(0, 3, '工作要求')
sheet.write(0, 4, '公司名称')
sheet.write(0, 5, '工作行业')

n = 1


def save_to_excel(soup):
    job_list = soup.find(class_='job-list').find_all('li')

    for item in job_list:
        job_name = item.find(class_='job-name').string
        job_area = item.find(class_='job-area').string
        job_salary = item.find(class_='job-limit clearfix').find(class_='red').string
        job_limit = item.find(class_='job-limit clearfix').find('p').text
        company_name = item.find(class_='info-company').find(class_='company-text').find(class_='name').string
        job_industry = item.find(class_='false-link').string

        print(
            'job：' + job_name + ' | ' + job_area + ' | ' + job_salary + ' | ' + job_limit + ' | ' + company_name + ' | ' + job_industry)

        global n

        sheet.write(n, 0, job_name)
        sheet.write(n, 1, job_area)
        sheet.write(n, 2, job_salary)
        sheet.write(n, 3, job_limit)
        sheet.write(n, 4, company_name)
        sheet.write(n, 5, job_industry)

        n = n + 1


def main(page):
    url = 'https://www.zhipin.com/c101040100/?query=%E9%87%87%E8%B4%AD&page=' + str(page) + '&ka=page-' + str(page)
    html = request_boss(url)
    soup = BeautifulSoup(html, 'lxml')
    save_to_excel(soup)


if __name__ == '__main__':
    main(1)

book.save(u'工作岗位.xlsx')
